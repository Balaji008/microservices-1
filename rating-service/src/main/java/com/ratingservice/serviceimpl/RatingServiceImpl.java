package com.ratingservice.serviceimpl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ratingservice.dto.RatingDto;
import com.ratingservice.entities.RatingEntity;
import com.ratingservice.repository.RatingRepository;
import com.ratingservice.service.RatingService;

@Service
public class RatingServiceImpl implements RatingService {

	@Autowired
	private RatingRepository ratingRepository;

	@Override
	public RatingDto addRating(RatingDto ratingDto) {
		RatingEntity ratingEntity = new RatingEntity();
		ratingEntity.setUserId(ratingDto.getUserId());
		ratingEntity.setHotelId(ratingDto.getHotelId());
		ratingEntity.setFeedback(ratingDto.getFeedback());
		ratingEntity.setRating(ratingDto.getRating());
		this.ratingRepository.save(ratingEntity);
		ratingDto.setId(ratingEntity.getId());
		return ratingDto;
	}

	@Override
	public List<RatingDto> getAllRatings() {
		// TODO Auto-generated method stub
		List<RatingEntity> ratings = this.ratingRepository.findAll();
		List<RatingDto> ratingDtos = new LinkedList<>();
		ratings.forEach(r -> {
			RatingDto ratingDto = new RatingDto();
			ratingDto.setId(r.getId());
			ratingDto.setUserId(r.getUserId());
			ratingDto.setHotelId(r.getHotelId());
			ratingDto.setFeedback(r.getFeedback());
			ratingDto.setRating(r.getRating());
			ratingDtos.add(ratingDto);
		});
		return ratingDtos;
	}

	@Override
	public List<RatingDto> getAllByUserId(Long userId) {
		List<RatingEntity> ratings = this.ratingRepository.findByUserId(userId);
		List<RatingDto> ratingDtos = new LinkedList<>();
		ratings.forEach(r -> {
			RatingDto ratingDto = new RatingDto();
			ratingDto.setId(r.getId());
			ratingDto.setUserId(r.getUserId());
			ratingDto.setHotelId(r.getHotelId());
			ratingDto.setFeedback(r.getFeedback());
			ratingDto.setRating(r.getRating());
			ratingDtos.add(ratingDto);
		});
		return ratingDtos;
	}

	@Override
	public List<RatingDto> getAllByHotelId(Long hotelId) {
		List<RatingEntity> ratings = this.ratingRepository.findByHotelId(hotelId);
		List<RatingDto> ratingDtos = new LinkedList<>();
		ratings.forEach(r -> {
			RatingDto ratingDto = new RatingDto();
			ratingDto.setId(r.getId());
			ratingDto.setUserId(r.getUserId());
			ratingDto.setHotelId(r.getHotelId());
			ratingDto.setFeedback(r.getFeedback());
			ratingDto.setRating(r.getRating());
			ratingDtos.add(ratingDto);
		});
		return ratingDtos;
	}

}
