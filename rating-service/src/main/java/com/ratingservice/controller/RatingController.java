package com.ratingservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ratingservice.dto.RatingDto;
import com.ratingservice.service.RatingService;

@RestController
@RequestMapping("/ratings")
public class RatingController {

	@Autowired
	private RatingService service;

	@PostMapping("/add-rating")
	public ResponseEntity<?> addRating(@RequestBody RatingDto ratingDto) {
		RatingDto rating = this.service.addRating(ratingDto);

//		return ResponseEntity.status(HttpStatus.CREATED).body(service.addRating(ratingDto));
		return new ResponseEntity<RatingDto>(rating, HttpStatus.CREATED);
	}

	@GetMapping("/all-ratings")
	public ResponseEntity<?> getRatings() {
		List<RatingDto> ratings = this.service.getAllRatings();

		return new ResponseEntity<List<RatingDto>>(ratings, HttpStatus.OK);
	}

	@GetMapping("/hotel-id/{hotelId}")
	public ResponseEntity<?> ratingsByHotelId(@PathVariable("hotelId") Long hotelId) {
		List<RatingDto> ratings = this.service.getAllByHotelId(hotelId);

		return new ResponseEntity<List<RatingDto>>(ratings, HttpStatus.OK);
	}

	@GetMapping("/user-id/{userId}")
	public ResponseEntity<?> ratingsByUserId(@PathVariable("userId") Long userId) {
		List<RatingDto> ratings = this.service.getAllByUserId(userId);

		return new ResponseEntity<List<RatingDto>>(ratings, HttpStatus.OK);
	}
}
