package com.ratingservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ratingservice.entities.RatingEntity;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {

	List<RatingEntity> findByUserId(Long userId);

	List<RatingEntity> findByHotelId(Long hotelId);
}
