package com.ratingservice.service;

import java.util.List;

import com.ratingservice.dto.RatingDto;

public interface RatingService {

	RatingDto addRating(RatingDto ratingDto);

	List<RatingDto> getAllRatings();

	List<RatingDto> getAllByUserId(Long userId);

	List<RatingDto> getAllByHotelId(Long hotelId);
}
