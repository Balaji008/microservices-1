package com.ratingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RatingDto {

	private Long id;

	private Long userId;

	private Long hotelId;

	private Float rating;

	private String feedback;
}
