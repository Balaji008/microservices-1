package com.userservice.service;

import java.util.List;

import com.userservice.dto.UserDto;

public interface UserService {

	UserDto saveUser(UserDto userDto);

	List<UserDto> getAllUsers();
	
	UserDto getUser(Long id);
}
