package com.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userservice.dto.UserDto;
import com.userservice.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping("/add-user")
	public ResponseEntity<?> addUser(@RequestBody UserDto userDto) {
		UserDto saveUser = this.service.saveUser(userDto);

		return new ResponseEntity<UserDto>(saveUser, HttpStatus.CREATED);
	}

	@GetMapping("/get-all-users")
	public ResponseEntity<?> getAllUsers() {
		List<UserDto> users = this.service.getAllUsers();

		return new ResponseEntity<List<UserDto>>(users, HttpStatus.CREATED);
	}

	@GetMapping("/get-user/{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id) {
		UserDto user = this.service.getUser(id);

		return new ResponseEntity<UserDto>(user, HttpStatus.CREATED);
	}

}
