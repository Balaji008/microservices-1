package com.userservice.dto;

import java.util.List;

import com.userservice.entities.RatingEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {

	private Long id;

	private String name;

	private String email;

	private List<RatingEntity> ratings;
}
