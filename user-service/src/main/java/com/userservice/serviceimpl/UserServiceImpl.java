package com.userservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.dto.UserDto;
import com.userservice.entities.UserEntity;
import com.userservice.exception.ResourceNotFoundException;
import com.userservice.repository.UserRepository;
import com.userservice.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;

	public UserDto saveUser(UserDto userDto) {
		UserEntity entity = new UserEntity();
		entity.setName(userDto.getName());
		entity.setEmail(userDto.getEmail());
		this.repository.save(entity);
		userDto.setId(entity.getId());
		return userDto;
	}

	public List<UserDto> getAllUsers() {
		List<UserEntity> users = this.repository.findAll();
		List<UserDto> userDtos = new ArrayList<>();
		users.forEach(u -> {
			UserDto userDto = new UserDto();
			userDto.setId(u.getId());
			userDto.setName(u.getName());
			userDto.setEmail(u.getEmail());
			userDto.setRatings(new ArrayList<>());
			userDtos.add(userDto);
		});
		return userDtos;
	}

	public UserDto getUser(Long id) {
		UserEntity userEntity = this.repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found", "User not found"));
		UserDto userDto = new UserDto();
		userDto.setId(userEntity.getId());
		userDto.setName(userEntity.getName());
		userDto.setEmail(userEntity.getEmail());
		return userDto;
	}

}
